from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Reviews
from .views import reviews
from .forms import review
from .apps import UlasanConfig
# Create your tests here.

class UlasanTest(TestCase): 
    def test_app(self):
        self.assertEqual(UlasanConfig.name, 'ulasan')  

    def test_kegiatan_exists(self):
        response = Client().get('/ulasan/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'ulasan.html')

# <<<<<<< HEAD
    def test_post(self):
        form = review(data={'pembeli':"aaaa",'rate':"dggd",'barang':"nama barang",'pesan':"adfdf"})
        self.assertTrue(form.is_valid())
        response_post = Client().post('/ulasan', {'pembeli':"aaaa",'rate':('1', 'Bintang Satu'),'barang':"nama barang",'pesan':"adfdf"})
 
        self.assertEqual(response_post.status_code, 302)
# =======
    def test_post(self):
        form = review(data={'pembeli':"aaaa",'rate':"dggd",'barang':"nama barang",'pesan':"adfdf"})
        self.assertTrue(form.is_valid())
        response_post = Client().post('/ulasan', {'pembeli':"aaaa",'rate':('1', 'Bintang Satu'),'barang':"nama barang",'pesan':"adfdf"})
# >>>>>>> 39f2cc22426174cce4dd018236a9b24c82810bd9
