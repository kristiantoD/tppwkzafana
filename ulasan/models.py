from django.db import models
from bartrans.models import trans

# Create your models here.
class Reviews(models.Model):
    pembeli = models.CharField(blank=False, max_length= 100)
    daftar_rate = (
        ('1', 'Bintang Satu'),
        ('2', 'Bintang Dua'),
        ('3', 'Bintang Tiga'),
        ('4', 'Bintang Empat'),
        ('5', 'Bintang Lima'),
    )
    daftar_barang = trans.objects.values_list('id','nama')
    rate = models.CharField(max_length=1)
    barang = models.CharField(max_length=30)
    pesan = models.CharField(blank=False, max_length= 100)
    tanggal = models.DateField('Date', auto_now_add=True)

    # trans = models.ForeignKey(trans, on_delete=models.CASCADE)
