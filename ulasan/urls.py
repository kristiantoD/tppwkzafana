from django.urls import path, include
from . import views

app_name = "reviewName"

urlpatterns = [
    # path('',views.index, name='ulasan'),
    path('ulasan/', views.reviews, name='reviews'),
]