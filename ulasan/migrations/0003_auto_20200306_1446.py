# Generated by Django 2.1.1 on 2020-03-06 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulasan', '0002_auto_20200306_1433'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviews',
            name='name',
        ),
        migrations.AlterField(
            model_name='reviews',
            name='barang',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='reviews',
            name='rate',
            field=models.CharField(max_length=1),
        ),
    ]
