from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pembeli', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=60)),
                ('rate', models.CharField(choices=[('1', 'Bintang Satu'), ('2', 'Bintang Dua'), ('3', 'Bintang Tiga'), ('4', 'Bintang Empat'), ('5', 'Bintang Lima')], max_length=1)),
                ('barang', models.CharField(max_length=100)),
                ('pesan', models.CharField(max_length=100)),
                ('tanggal', models.CharField(max_length=100)),
            ],
        ),
    ]
