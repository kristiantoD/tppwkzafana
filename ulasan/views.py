from django.shortcuts import render, redirect
from .models import Reviews
from .forms import review

# Create your views here. 
# def index(request):
#     qs = Reviews.objects.all()
#     return render(request, 'ulasan.html', {'review':review})

def reviews(request):
    if(request.method == "POST"):
        tmp = review(request.POST)
        pembeli = request.POST["pembeli"]
        rate = request.POST["rate"]
        barang = request.POST["barang"]
        pesan = request.POST["pesan"]
        tmp2 = Reviews(pembeli=pembeli, rate=rate, barang=barang,pesan=pesan)
            # tmp2.tanggal = tmp.cleaned_data["tanggal"]
        tmp2.save()
        return redirect("/ulasan")
    else:
        tmp = review()
        tmp2 = Reviews.objects.all()
        tmp_dictio = {
            'review' : tmp,
            'reviews' : tmp2
        }
        #sprint("ini tmp 2 : ",tmp2)
        return render(request, 'ulasan.html', tmp_dictio)
