from django import forms
from bartrans.models import trans

daftar_rate = [
        ('1', 'Bintang Satu'),
        ('2', 'Bintang Dua'),
        ('3', 'Bintang Tiga'),
        ('4', 'Bintang Empat'),
        ('5', 'Bintang Lima'),
]
daftar_barang = trans.objects.values_list('nama','nama')

class review(forms.Form):
    pembeli = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Pembeli',
        'type' : 'text',
        'required' : True
    }))
    rate = forms.CharField(label='Pilih Rate', widget=forms.Select(choices=daftar_rate , attrs={
    'class' : "form-control",
    'required' : True
    }))
    barang = forms.CharField(label='Pilih Barang', widget=forms.Select(choices=daftar_barang , attrs={
    'class' : "form-control",
    'required' : True
    }))
    pesan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pesan',
        'type' : 'text',
        'required' : True
    }))
    # tanggal = forms.DateField(widget=forms.SelectDateWidget(attrs={
    #     'class' : 'form-control',
    #     'placeholder' : 'Pilih Tanggal',
    #     'required' : True
    # }))
    # tanggal = forms.DateField(label='What is your birth date?', widget=forms.SelectDateWidget)