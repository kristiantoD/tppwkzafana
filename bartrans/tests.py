from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import beli, cekharga, transaksi, transc, transaksiku
from .models import trans, mytrans
from MainMenu.models import Barang
from coupon.models import Coupon
from .forms import FormBeli
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from .apps import BartransConfig

# Create your tests here. 
#get manggil html
#resolve func dipanggil
#
class storyTest(TestCase):       
    def test_apps(self):
        self.assertEqual(BartransConfig.name, 'bartrans')    

    def test_kegiatan_resolv(self):
        url = reverse('transaksi')
        self.assertEquals(resolve(url).func,transaksi)


    def test_kegiatan_exists(self):
        response = self.client.get(reverse('transaksi'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'transaksi.html')
        
    def test_app_func(self):
        Barang.objects.create(nama="paa", image ="pac", harga=999 ,stok=10)
        found=resolve('/beli/1')
        self.assertEqual(found.func, beli)
        
    def test_model_cek(self):
        trans.objects.create(nama="paa", tanggal =timezone.now(), kupon ="pab", image ="pac", harga=99990 ,barang="pae")
        hitungjumlah = trans.objects.all().count()
        self.assertEqual(hitungjumlah,1)
        self.assertEquals(str(trans.objects.get(pk=1)),'paa')
        
    def test_using_right_template(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=999 ,stok=10)
        uu.save()
        response=Client().get('/beli/1')
        self.assertTemplateUsed(response, 'beli.html')
        self.assertEqual(response.status_code,200)

    def test_story6_post_kuponexp(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=100000 ,stok=10)
        uu.save()
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now(), diskon=50 ,minimum = 20000)
        ku.save()
        form = FormBeli(data={'nama':"test",'kupon':"DISKON50",'cek':"cek"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/beli/cek/1', {'nama':"test",'kupon':"DISKON50",'cek':"cek"})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'beli.html')

    def test_story6_post_kupontakada(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=100000 ,stok=10)
        uu.save()
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now(), diskon=50 ,minimum = 20000)
        ku.save()
        form = FormBeli(data={'nama':"test",'kupon':""})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/beli/cek/1', {'nama':"test",'kupon':""})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'beli.html')

    def test_story6_post_kuponkurang_harga(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=10000 ,stok=10)
        uu.save()
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now()+ timezone.timedelta(days=2), diskon=50 ,minimum = 20000)
        ku.save()
        form = FormBeli(data={'nama':"test",'kupon':"DISKON50"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/beli/cek/1', {'nama':"test",'kupon':"DISKON50"})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'beli.html')

    def test_story6_post_kupontaktemu(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=100000 ,stok=10)
        uu.save()
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now()+ timezone.timedelta(days=2), diskon=50 ,minimum = 20000)
        ku.save()
        form = FormBeli(data={'nama':"test",'kupon':"ffff",'beli':"beli"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/beli/cek/1', {'nama':"test",'kupon':"ffff",'beli':"beli"})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'beli.html')

    def test_story6_post_kuponberhasil(self):
        uu = Barang.objects.create(nama="paa", image ="pac", harga=100000 ,stok=10)
        uu.save()
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now()+ timezone.timedelta(days=2), diskon=50 ,minimum = 20000)
        ku.save()
        form = FormBeli(data={'nama':"test",'kupon':"DISKON50",'beli':"beli"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/beli/cek/1', {'nama':"test",'kupon':"DISKON50",'beli':"beli"})
        self.assertEqual(response_post.status_code, 302)


    def test_model_mytrans(self):
        user = User.objects.create_user('pewe','pewe@ppw.com', 'ppwpassword')
        user.save()
        mytrans.objects.create(user=User.objects.get(username='pewe'), tanggal =timezone.now(), kupon ="pab", image ="pac", harga=99990,jumlah=2,hargadisc=100 ,barang="pae")
        hitungjumlah = mytrans.objects.all().count()
        self.assertEqual(hitungjumlah,1)
        self.assertEquals(str(mytrans.objects.get(pk=1)),'pewe')

    def test_transc_resolv(self):
        url = reverse('transc')
        self.assertEquals(resolve(url).func,transc) 

    def test_transaksiku_resolv(self):
        url = reverse('transaksiku')
        self.assertEquals(resolve(url).func,transaksiku)

    # def test_transku_exists(self):
    #     self.credentials = {
    #         'username': 'testuser',
    #         'password': 'secret'}
    #     User.objects.create_user(**self.credentials)
    #     response = self.client.post('/login/', self.credentials, follow=True)
    #     response1 = self.client.get(reverse('transaksiku'))
    #     self.assertEquals(response1.status_code, 200)
    #     self.assertTemplateUsed(response1,'transaksiku.html')

    # def test_transc_post(self):
    #     uu = Barang.objects.create(nama="paa", image ="pac", harga=100000 ,stok=10)
    #     uu.save()
    #     up = Barang.objects.create(nama="pab", image ="pad", harga=10 ,stok=10)
    #     up.save()
    #     ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now()+ timezone.timedelta(days=2), diskon=50 ,minimum = 20000)
    #     ku.save()
    #     ks = Coupon.objects.create(kode="DISKON20", tanggal =timezone.now(), diskon=20 ,minimum = 20000)
    #     ks.save()
    #     response_post = self.client.post(reverse('transc'), {'brg[]':"paa",'jml[]':"1",'hrg[]':"100000", 'kup':"DISKON50", 'tot':"100010"})
    #     self.assertEqual(response_post.status_code, 302)



