from django.shortcuts import render, redirect 
from django.http import HttpResponse 
from .forms import FormBeli
from .models import trans, mytrans
from coupon.models import Coupon
from MainMenu.models import Barang
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib import messages
from django.contrib.auth.models import User
# Create your views here.

def transaksi(request):
	data = trans.objects.all()
	return render(request,"transaksi.html",{'data':data})
	
def beli(request,pk):
	data = Barang.objects.get(pk=pk)
	context = {
		"data":data,
		"form":FormBeli,
		"kata":""
	}
	if request.user.is_authenticated:
		return render(request,"beli.html",{'data':data})
	else:
		return render(request,"beli.html", context)

def cekharga(request,pk):
	i=0
	form = FormBeli(request.POST)
	if request.method == 'POST':
		kode = request.POST['kupon']
		nama = request.POST['nama']
	data = Barang.objects.get(pk=pk)
	harga = data.harga
	if kode:
		try:
			Coupon.objects.get(kode=kode)
			kupon1 = Coupon.objects.get(kode=kode)
			now = timezone.now()
			if kupon1.minimum>data.harga:
				context={
					"kata":"Total harga: Rp."+str(harga),
					"data":data,
					"form":form
				}
				messages.add_message(request, messages.WARNING,"Harga barang kurang untuk menggunakan kupon ini.")
			elif kupon1.tanggal<now:
				context={
					"kata":"Total harga: Rp."+str(harga),
					"data":data,
					"form":form
				}
				messages.add_message(request, messages.WARNING,"Kupon yang anda gunakan telah expired.")
			else:
				i=1
				harga = data.harga*(100-kupon1.diskon)/100
				context={
					"kata":"Total harga: Rp."+str(harga),
					"data":data,
					"form":form
					}
				messages.add_message(request, messages.SUCCESS,"Kupon berhasil digunakan")
		except:
			context={
				"kata":"Total harga: Rp."+str(harga),
				"data":data,
				"form":form
			}
			messages.add_message(request, messages.WARNING,"Kupon yang anda masukkan tidak ditemukan.")
	else:
		i=1
		context={
			"kata":"Total harga: Rp."+str(data.harga),
			"data":data,
			"form":form
		}
		messages.add_message(request, messages.INFO,"Tidak ada kupon yang digunakan.")

	if 'cek' in request.POST:
		return render(request,"beli.html",context)
	elif 'beli' in request.POST:
		if i==1:
			mytrans = trans(nama = nama,tanggal = now, kupon = kode, image = data.image, harga = harga, barang = data.nama)
			mytrans.save()
			return redirect("transaksi")
		else:
			context={
				"kata":"Total harga: Rp."+str(harga),
				"data":data,
				"form":form
			}
			messages.add_message(request, messages.WARNING,"Kupon gagal digunakan, tidakbisa membeli.")
			return render(request,"beli.html",context)

	else:
		return render(request,"beli.html",context)

#	return render(request,"coupon.html",context)

def transc(request):
	if request.method == 'POST':
		barang = request.POST.getlist('brg[]')
		jumlah = request.POST.getlist('jml[]')
		harga = request.POST.getlist('hrg[]')
		kode = request.POST['kup']
		tot = request.POST['tot']
	for i in range(len(barang)):
		if int(Barang.objects.get(nama=barang[i]).stok) < int(jumlah[i]):
			return HttpResponse("Barang yang akan anda beli melebihi stok yang ada")

	if kode:
		try:
			kupon1 = Coupon.objects.get(kode=kode)
			a=kupon1.diskon
			now = timezone.now()
			if kupon1.minimum>int(tot):
				return HttpResponse("Harga barang kurang untuk menggunakan kupon ini.")
			elif kupon1.tanggal<now:
				return HttpResponse("Kupon yang anda gunakan telah expired.")
		except:
			return HttpResponse("Kupon yang anda masukkan tidak ditemukan.")
	else:
		a=0


	for i in range(len(barang)):
		item = Barang.objects.get(nama=barang[i])
		item.stok=str(int(item.stok)-int(jumlah[i]))
		item.save()
		trans = mytrans(tanggal = timezone.now(), kupon = kode, image = Barang.objects.get(nama=barang[i]).image, harga = harga[i], barang = barang[i], jumlah=jumlah[i], hargadisc=int(harga[i])*int(jumlah[i])*(100-a)/100, user=request.user)
		trans.save()

	return HttpResponse("Semua barang berhasil dibeli suskes")

def transaksiku(request):
	data = mytrans.objects.filter(user=request.user)
	return render(request,"transaksiku.html",{'data':data})
