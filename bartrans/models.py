from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User
# Create your models here.


class trans(models.Model):
	nama = models.CharField(max_length=100)
	tanggal = models.DateTimeField()
	kupon = models.CharField(max_length=50)
	image = models.ImageField(upload_to='static/images/')
	harga = models.IntegerField(validators=[MinValueValidator(0)])
	barang = models.CharField(max_length=50)
	def __str__(self):
		return self.nama

class mytrans(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	tanggal = models.DateTimeField()
	kupon = models.CharField(max_length=50)
	image = models.ImageField(upload_to='static/images/')
	harga = models.IntegerField(validators=[MinValueValidator(0)])
	jumlah = models.PositiveIntegerField()
	hargadisc = models.IntegerField(validators=[MinValueValidator(0)])
	barang = models.CharField(max_length=50)
	def __str__(self):
		return self.user.username