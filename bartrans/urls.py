from django.urls import path
from .views import beli, cekharga, transaksi, transc, transaksiku
 
urlpatterns = [ 
	path('beli/<int:pk>', beli, name = 'beli'),
	path('beli/cek/<int:pk>',cekharga, name = 'cekharga'),
	path('transaksi/',transaksi,name = 'transaksi'),
	path('transc/',transc,name = 'transc'),
	path('transaksiku/',transaksiku,name = 'transaksiku'),
] 
