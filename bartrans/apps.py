from django.apps import AppConfig


class BartransConfig(AppConfig):
    name = 'bartrans'
