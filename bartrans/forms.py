from django import forms

class FormBeli(forms.Form):
	nama = forms.CharField(max_length=100,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan nama anda'
								}))
	kupon = forms.CharField(max_length=30, required=False,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'Masukkan kode kupon'
								}))

	