from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core.serializers import serialize
# from django.core import serializers
from .models import Coupon

 
def index(request):
    qs = Coupon.objects.all()
    context = {
        'queryset' : qs,
    }
    return render(request, 'coupon.html', context)

def searchObjects(request):
    allObject_list = list(Coupon.objects.values())
    print(allObject_list)
    # serialize('json', allObject_list)
    # isi = serialize('json', allObject_list)
    return JsonResponse(allObject_list, safe=False)