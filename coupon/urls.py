from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='coupon'),
    path('search/', views.searchObjects),
]