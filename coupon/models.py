from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
import datetime

# Create your models here.
class Coupon(models.Model):
    kode = models.CharField(max_length=50,unique=True)
    tanggal = models.DateTimeField()
    diskon = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])
    minimum = models.IntegerField()

    def __str__(self):
        return self.kode

    def as_json(self):
        return dict(
            id=self.id, kode=self.kode,
            tanggal=self.tanggal.isoformat(), 
            diskon=self.diskon,
            minimum=self.minimum)
