from django.test import TestCase, Client, LiveServerTestCase
from django.utils import timezone
from django.urls import reverse, resolve
from .models import Coupon
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from .apps import CouponConfig
# Create your tests here.

class UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(CouponConfig.name, 'coupon') 

    def test_model_create_new_coupon(self):
        new_coupon = Coupon.objects.create(kode="xjahfjh",diskon=40,tanggal=timezone.now(),minimum=400000)
        banyak = Coupon.objects.all().count()
        self.assertEqual(banyak,1)
        new_coupon_2 = Coupon.objects.create(kode="ajhfas",diskon=20,tanggal=timezone.now(),minimum=50000)
        banyak2 = Coupon.objects.all().count()
        self.assertEqual(banyak2,2)
        self.assertEquals(str(Coupon.objects.get(pk=1)),'xjahfjh')

    def test_index_exists(self):
        response = self.client.get(reverse('coupon'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'coupon.html')

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
    
    def test_main(self):
        ku = Coupon.objects.create(kode="DISKON50", tanggal =timezone.now(), diskon=50 ,minimum = 20000)
        self.selenium.get(self.live_server_url + '/coupon')
        a = self.selenium.get(self.live_server_url + '/coupon')
        # print("aaaaaaaaaaaaaa = ",a)

        #Test First Login (didn't register)
        self.selenium.find_element_by_id('myInput').send_keys('50')
        time.sleep(5)
        self.assertIn('DISKON50', self.selenium.page_source)