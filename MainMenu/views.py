from django.shortcuts import render
from django.shortcuts import HttpResponse
from .models import Barang
# Create your views here.

def Home(request):
        qs = Barang.objects.all()
        context ={
                'queryset' : qs ,
        }
        return render(request,'home.html', context)

