from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Barang(models.Model):
    image = models.ImageField(upload_to='static/images/')
    nama = models.CharField(max_length=50)
    harga = models.IntegerField(validators=[MinValueValidator(0)])
    stok = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
