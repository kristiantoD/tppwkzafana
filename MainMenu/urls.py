from django.urls import path
from .views import  Home

app_name ='mainmenu'

urlpatterns = [
	path('', Home, name = 'home'),
]
